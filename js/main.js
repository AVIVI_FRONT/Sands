/*

    Template:  Sheltek Real Estate HTML5 Template
    Author: http://devitems.com/
    Version: 1
    Design and Developed by: http://devitems.com/
    NOTE: If you have any note put here. 

*/
/*================================================
[  Table of contents  ]
================================================
	1. jQuery MeanMenu
	2. wow js active
	3. scrollUp jquery active
    4. Nivo Slider 
    5. Price Slider
	6. slick carousel 
    6. tooltip
    7. Service Carousel
    8. Agents Carousel
    9. Testimonial Carousel
    10. Blog Carousel
    11. Brand Carousel
    12. Blog Carousel
    13. counter
    14. Background Toutube Video 
    15. STICKY sticky-header
 
======================================
[ End table content ]
======================================*/


(function($) {
    "use strict";

    /*-------------------------------------------
        1. jQuery MeanMenu
    --------------------------------------------- */
    jQuery('nav#dropdown').meanmenu();

    /*-------------------------------------------
        2. wow js active
    --------------------------------------------- */
    new WOW().init();

    /*-------------------------------------------
        3. scrollUp jquery active
    --------------------------------------------- */
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });

    /*-------------------------------------------
        4. Nivo Slider
    --------------------------------------------- */
    $('#ensign-nivoslider-3').nivoSlider({
        // effect: 'fade',
        effect: 'random',
        slices: 15,
        boxCols: 8,
        boxRows: 4,
        animSpeed: 500,
        pauseTime: 5000,
        prevText: 'p<br/>r<br/>e<br/>v', 
        nextText: 'n<br/>e<br/>x<br/>t',
        startSlide: 0,
        directionNav: true,
        controlNav: true,
        controlNavThumbs: false,
        pauseOnHover: true,
        manualAdvance: false
    });

    /* ********************************************
        5. Price Slider
    ******************************************** */
    $( "#slider-range" ).slider({
        range: true,
        min: 20,
        max: 2500,
        values: [ 80, 2000 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
    " - $" + $( "#slider-range" ).slider( "values", 1 ) ); 

    /* ********************************************
        5.1 Area Slider
    ******************************************** */
    $( "#slider-range-area" ).slider({
        range: true,
        min: 10,
        max: 4000,
        values: [ 200, 3000 ],
        slide: function( event, ui ) {
            $( "#amount-area" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] + " sqft");
        }
    });
    $( "#amount-area" ).val( $( "#slider-range-area" ).slider( "values", 0 ) + " - " + $( "#slider-range-area" ).slider( "values", 1 ) + " sqft");

    /* ********************************************
        5.2 Price Slider
    ******************************************** */
    $( "#slider-range-living" ).slider({
        range: true,
        min: 30,
        max: 400,
        values: [ 35, 350 ],
        slide: function( event, ui ) {
            $( "#amount-living" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] + " sqft");
        }
    });
    $( "#amount-living" ).val( $( "#slider-range-living" ).slider( "values", 0 ) + " - " + $( "#slider-range-living" ).slider( "values", 1 ) + " sqft");

    /* ********************************************
        5.3 Terrace Slider
    ******************************************** */
    $( "#slider-terrace" ).slider({
        range: true,
        min: 200,
        max: 600,
        values: [ 150, 550 ],
        slide: function( event, ui ) {
            $( "#amount-terrace" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] + " sqft");
        }
    });
    $( "#amount-terrace" ).val( $( "#slider-terrace" ).slider( "values", 0 ) + " - " + $( "#slider-terrace" ).slider( "values", 1 ) + " sqft");


    /*************************
        6. tooltip
    *************************/
    $('[data-toggle="tooltip"]').tooltip();

    /*************************
        7. Service Carousel
    *************************/ 
    $('.service-carousel').slick({
        arrows: false,
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 3,
        responsive: [
            { breakpoint: 991, settings: { slidesToShow: 3, slidesToScroll: 2 } }, // Tablet
            { breakpoint: 767, settings: { slidesToShow: 2, slidesToScroll: 1 } }, // Large Mobile
            { breakpoint: 479, settings: { slidesToShow: 1, slidesToScroll: 1 } }  // Small Mobile
        ]
    });

    /*************************
     7.1. Dveej Carousel
     *************************/

    $('.new-tab-slider').slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true
    });


    /*************************
        8. Agents Carousel
    *************************/ 
    $('.agents-carousel').slick({
        arrows: false,
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 3,
        responsive: [
            { breakpoint: 991, settings: { slidesToShow: 3, slidesToScroll: 2 } }, // Tablet
            { breakpoint: 767, settings: { slidesToShow: 1, slidesToScroll: 1 } }, // Large Mobile
            { breakpoint: 479, settings: { slidesToShow: 1, slidesToScroll: 1 } }  // Small Mobile
        ]
    });

    /*************************
        9. Testimonial Carousel
    *************************/ 
    $('.testimonial-carousel').slick({
        arrows: false,
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    /*************************
        10. Blog Carousel
    *************************/ 
    $('.blog-carousel').slick({
        arrows: false,
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 2,
        responsive: [
            { breakpoint: 991, settings: { slidesToShow: 2, slidesToScroll: 1 } }, // Tablet
            { breakpoint: 767, settings: { slidesToShow: 1, slidesToScroll: 1 } }, // Large Mobile
            { breakpoint: 479, settings: { slidesToShow: 1, slidesToScroll: 1 } }  // Small Mobile
        ]
    });

    /*************************
        11. Brand Carousel
    *************************/ 
    $('.brand-carousel').slick({
        arrows: false,
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 4,
        responsive: [
            { breakpoint: 1169, settings: { slidesToShow: 4, slidesToScroll: 3 } }, // Medium Device
            { breakpoint: 991, settings: { slidesToShow: 3, slidesToScroll: 2 } }, // Tablet
            { breakpoint: 767, settings: { slidesToShow: 2, slidesToScroll: 1 } }, // Large Mobile
            { breakpoint: 479, settings: { slidesToShow: 1, slidesToScroll: 1 } }  // Small Mobile
        ]
    });

    /*************************
        12. Blog Carousel
    *************************/ 
    $('.pro-details-carousel').slick({
        arrows: false,
        dots: false,
        vertical: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 3,
        responsive: [
            { breakpoint: 991, settings: { slidesToShow: 4, slidesToScroll: 3 } }, // Tablet
            { breakpoint: 767, settings: { slidesToShow: 3, slidesToScroll: 2 } }, // Large Mobile
            { breakpoint: 479, settings: { slidesToShow: 2, slidesToScroll: 2 } }  // Small Mobile
        ]
    });

    /*************************
        13. counter
    *************************/ 
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
    
    /* ********************************************
        14. Background Toutube Video 
    ******************************************** */
    $(".youtube-bg").YTPlayer({
        videoURL:"Sz_1tkcU0Co",
        containment:'.youtube-bg',
        mute:true,
        loop:true,
    });

    /* ********************************************
        14.1. Property slider
    ******************************************** */

    $(".property__slider-nav--vertical").slick({
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 8,
        slidesToScroll: 1,
        focusOnSelect: true,
        autoplay: false,
        infinite: false,
        prevArrow: '<span class="ion-ios-arrow-up slick-vertical-arrow slick-vertical-prev-arrow"></span>',
        nextArrow: '<span class="ion-ios-arrow-down slick-vertical-arrow slick-vertical-next-arrow"></span>',
        asNavFor: '.property__slider-images',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    prevArrow: '<span class="ion-ios-arrow-left slick-horizontal-arrow slick-horizontal-prev-arrow"></span>',
                    nextArrow: '<span class="ion-ios-arrow-right slick-horizontal-arrow slick-horizontal-next-arrow"></span>',
                    vertical: false,
                    verticalSwiping: false,
                    slidesToShow: 6,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    prevArrow: '<span class="ion-ios-arrow-left slick-horizontal-arrow slick-horizontal-prev-arrow"></span>',
                    nextArrow: '<span class="ion-ios-arrow-right slick-horizontal-arrow slick-horizontal-next-arrow"></span>',
                    vertical: false,
                    verticalSwiping: false,
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 479,
                settings: {
                    prevArrow: '<span class="ion-ios-arrow-left slick-horizontal-arrow slick-horizontal-prev-arrow"></span>',
                    nextArrow: '<span class="ion-ios-arrow-right slick-horizontal-arrow slick-horizontal-next-arrow"></span>',
                    vertical: false,
                    verticalSwiping: false,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
        ]
    });

    $(".property__slider-nav--horizontal").slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        focusOnSelect: true,
        autoplay: false,
        infinite: false,
        prevArrow: '<span class="ion-ios-arrow-left slick-horizontal-arrow slick-horizontal-prev-arrow"></span>',
        nextArrow: '<span class="ion-ios-arrow-right slick-horizontal-arrow slick-horizontal-next-arrow"></span>',
        asNavFor: '.property__slider-images',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 479,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
        ]
    });

    $(".property__slider-images").on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        var i = (currentSlide ? currentSlide : 0) + 1;
        $(".sliderInfo").text(i + "/" + slick.slideCount + " Photos");
    });

    $(".image-navigation__prev").on('click', function () {
        $(".property__slider-images").slick('slickPrev');
    });

    $(".image-navigation__next").on('click', function () {
        $(".property__slider-images").slick('slickNext');
    });

    $(".property__slider-images").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        infinite: false,
        arrows: false,
        fade: true,
        asNavFor: '.property__slider-nav'
    });

    /* ********************************************
    16. Property Tabs
    ******************************************** */

    $(".js-navigation-tab").on('click', function() {
        $(".js-navigation-tab").removeClass('active');
        $(this).addClass('active');

        var dataTab = "." + $(this).data('tab');

        $('.property__tab-block').hide().removeClass('active');
        $(dataTab).fadeIn();

        if ($(window).width()<768) {
            setTimeout(function(){
                var scroller=$('.js-navigation-tab.active').offset().top - 20;
                $('html, body').scrollTop(scroller);
            },310)
        }

    });

    /* ********************************************
    17. Phone mask
    ******************************************** */

    if ($('body .js-phone').length > 0) {
        jQuery(function($){
            $(".js-phone").mask("+38 (999) 999-9999");
        });
    }


    /* ********************************************
    18. Property Accordion
    ******************************************** */
    function sliderFix(lm) {
        if ($(lm).length > 0) {
            setTimeout(function () {
                $(lm).slick('setPosition');
            }, 350);
        }
    }

    // function toggleTabs() {
        $('.blue-button').on('click', function (e) {
            e.stopPropagation();
            if ($(this).hasClass('active')) {
                $('.blue-button').addClass('active');
                var tab_id = $(this).siblings('.blue-button').data('tab');
                $(this).removeClass('active');
            } else {
                $('.blue-button').removeClass('active');
                $(this).addClass('active');
                tab_id = $(this).data('tab');
            }
            $('.custom-tab').removeClass('active');
            $('.custom-tab[data-tab="'+tab_id+'"]').addClass('active');
            sliderFix('.new-tab-slider-o');
            sliderFix('.navigate-new-tab-slider');
        })
    // }


    $(".property__accordion").on('click', '.property__accordion-header', function () {
        $(this).toggleClass('action');
        $(this).next().slideToggle(350);
        $(this).find('.property__accordion-expand').toggleClass('fa-caret-up fa-caret-down');
        $(this).parent().siblings().find('.property__accordion-content').slideUp(350);
        $(this).parent().siblings().find('.property__accordion-expand').removeClass('fa-caret-up').addClass('fa-caret-down');
        sliderFix('.property__accordion .new-tab-slider');
        sliderFix('.new-tab-slider-o');
        sliderFix('.navigate-new-tab-slider');
    });

    $('.start-slider').on('click', function () {
        sliderFix('.property__accordion .new-tab-slider');
    });




    // arrows: false,
    //     dots: false,
    //     infinite: true,
    //     speed: 300,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,

    $('.new-tab-slider-o').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        fade: false,
        asNavFor: '.navigate-new-tab-slider',
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>'
    });
    $('.navigate-new-tab-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.new-tab-slider-o',
        dots: false,
        // centerMode: true,
        focusOnSelect: true,
        arrows: false,
        swipeToSlide: true,
        infinite: false
    });

    function chooseBg() {
        var dec = Math.random()*10;
        if ((dec >= 0) && (dec <= 3)) {
            var x = 0;
        } else if ((dec > 3) && (dec <= 6)){
            x = 1;
        } else { x = 2; }
        $('.random-picture').eq(x).addClass('active');
    }

    /* ********************************************
    19. Points on map
    ******************************************** */

    $(document).ready(function () {
        chooseBg();
        // toggleTabs();
        var point = $('.properties__point');
        point.each(function () {
            $(this).css({
                left: $(this).data('pos-left'),
                top: $(this).data('pos-top')
            });
        });

        $('.js-properties-point').on('click', function () {
            point.removeClass('active');
            $(this).parents('.properties__point').addClass('active');

            var dataItem = $(this).parents('.properties__point').data('item');
            $('.featured-flat .flat-item').removeClass('hovered');
            $('.featured-flat .flat-item[data-item="' + dataItem +'"]').addClass('hovered');
        });

        $('.listing__close').on('click', function () {
            point.removeClass('active');
            $('.featured-flat .flat-item').removeClass('hovered');
        });

    });

    /* ********************************************
    20. Listing Search filter properties.html
    ******************************************** */

    $(".listing-search__btn").on('click', function () {

        $(this).toggleClass("js-hide");
        if ($(this).hasClass("js-hide")) {
            $(this).text("Hide");
        } else {
            $(this).text("Advance Search");
        }

        $(".listing-search__advance").slideToggle();

        return false;
    });

    $(".listing-search__more-btn").on('click', function () {
        $(this).toggleClass('listing-search__more-btn--show');
        $(".listing-search__more-inner").slideToggle();
        return false;
    });

    $(".main-listing__form-more-filter").on('click', function () {

        $(this).toggleClass("js-hide");
        if ($(this).hasClass("js-hide")) {
            $(this).text("Less Filter");
        } else {
            $(this).text("More Filter");
        }

        $(".main-listing__form-expand").slideToggle();

        return false;
    });


    /* ********************************************
    21. jQuery Custom Scrollbar properties.html
    ******************************************** */

    // $(".js-map-wrapper-scroll").mCustomScrollbar({
    //     axis: "x",
    //     theme: "dark-3",
    //     scrollInertia: 100,
    //     documentTouchScroll: false,
    //     contentTouchScroll: 25
    // });


    /* ********************************************
    22. Map controls properties.html
    ******************************************** */

    var mapWrap = $('.featured-flat-map');

    $('.js-map-fullscr').on('click', function () {
            $(".js-map-wrapper-scroll").mCustomScrollbar("destroy");
            $('.featured-flat-map-wrapper').addClass('fullscr').mCustomScrollbar({
                theme: "dark-3",
                scrollInertia: 100,
                documentTouchScroll: false,
                contentTouchScroll: 25
            });
            $('body').addClass('overflow');
    });

    $('.js-map-close').on('click', function () {
        $('.featured-flat-map-wrapper').removeClass('fullscr').mCustomScrollbar("destroy");
        $('body').removeClass('overflow');
        // $('.featured-flat .flat-item').removeClass('hovered');
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            if ($('.featured-flat-map-wrapper').hasClass('fullscr')) {
                $('.featured-flat-map-wrapper').removeClass('fullscr').mCustomScrollbar("destroy");
                $('body').removeClass('overflow');
                // $('.featured-flat .flat-item').removeClass('hovered');
            }
        }
    });

    $('.js-map-reset').on('click', function () {
        $('.featured-flat-map-wrapper').removeClass('fullscr').mCustomScrollbar("destroy");
        $('body').removeClass('overflow');
        $('.properties__point').removeClass('active');
        $('.featured-flat .flat-item').removeClass('hovered');
    });

    $('.js-map-next').on('click', function () {
        var nextItem = $('.properties__point.active').next('.properties__point').data('item');
        $('.properties__point.active').removeClass('active').next('.properties__point').addClass('active');
        $('.featured-flat .flat-item').removeClass('hovered');
        $('.featured-flat .flat-item[data-item="' + nextItem +'"]').addClass('hovered');
    });

    $('.js-map-prev').on('click', function () {
        var prevItem = $('.properties__point.active').prev('.properties__point').data('item');
        $('.properties__point.active').removeClass('active').prev('.properties__point').addClass('active');
        $('.featured-flat .flat-item').removeClass('hovered');
        $('.featured-flat .flat-item[data-item="' + prevItem +'"]').addClass('hovered');
    });

    /* ********************************************
    23. Hover ITEMS properties.html
    ******************************************** */

    $( ".featured-flat .flat-item" ).hover(
        function() {
            $('.featured-flat .flat-item').removeClass('hovered');
            $('.properties__point').removeClass('active');
            var itemNum = $(this).data('item');
            $('.featured-flat-map .properties__point[data-item="' + itemNum + '"]').addClass('active');

        }, function() {

        }
    );

    /* ********************************************
    24. Language select
    ******************************************** */

    $('.js-lang-select').on('click', function (e) {
        e.preventDefault();
        $('.js-lang-select').removeClass('active');
        $(this).addClass('active');
    });

    /* ********************************************
    25. Share button
    ******************************************** */

    $('.js-share-btn').on('click', function () {
        $('.share__popup').toggleClass('active');
        $('.js-overlay-click').toggle();
    });

    /* ********************************************
    26. Favorites
    ******************************************** */

    $('.js-favorites').on('click', function () {
        $('.header-favorite__popup').toggle();
        $('.js-overlay-click').toggle();
    });

    $('.js-overlay-click').on('click', function () {
        $('.header-favorite__popup').hide();
        $('.share__popup').removeClass('active');
        $('.js-overlay-click').hide();
    });










})(jQuery);

/* ********************************************
    15. STICKY sticky-header
******************************************** */
    var hth = $('.header-top-bar').height();
    $(window).on('scroll', function() {
        if ($(this).scrollTop() > hth){  
            $('#sticky-header').addClass("sticky");
        }
        else{
            $('#sticky-header').removeClass("sticky");
        }
    });
/* ********************************************************* */


